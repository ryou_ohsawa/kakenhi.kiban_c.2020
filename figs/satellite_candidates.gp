#!/usr/bin/env gnuplot
set terminal pdfcairo font 'M+ 1c,16' size 14cm,10cm
set output 'satellite_candidates.pdf'
set enc iso


f = '< sed "1d;2d" satellite_candidates_20201005.tbl'
# 01: NEO_SCO
# 02: NEO_SNR
# 03: NEO_UTC
# 04: NEO_RA
# 05: NEO_DEC
# 06: NEO_UTC1
# 07: NEO_RA1
# 08: NEO_DEC1
# 09: NEO_UTC2
# 10: NEO_RA2
# 11: NEO_DEC2
# 12: SAT_COUNT

wrapd_label(x) = (x>=360?wrapd_label(x-360):x<0?wrapd_label(x+360):x)
wrapd(x) = (x>180?wrapd(x-360):x<-180?wrapd(x+360):x)
cosd(x) = cos(pi*x/180.)
sind(x) = sin(pi*x/180.)
hammer_n(l,b) = sqrt(1+cosd(b)*cosd(l/2.))
hammer_x(l,b) = 2*sqrt(2)*cosd(b)*sind(l/2.)/hammer_n(l,b)
hammer_y(l,b) = sqrt(2)*sind(b)/hammer_n(l,b)

set tmargin 0
set bmargin 1
set lmargin 2
set rmargin 2

#set size ratio -1
set parametric
set tr [0:1]
set xtics auto
set ytics auto
set xr [-2.60:2.60]
set yr [-0.65:1.35]
unset xlabel
unset ylabel
unset xtics
unset ytics
unset border

labelcolor='black'
set label 1 'Right Ascension (deg)' font 'M+ 1c bold,12' \
front center at screen 0.5, screen 0.0 \
offset 0,0.5 tc rgb labelcolor
set label 2 'Declination (deg)' font 'M+ 1c bold,12' \
front center at screen 0.0, first hammer_y(180,0) \
rotate by 90 offset 0.8,0 tc rgb labelcolor

ra_shift=30
do for [i=-3:3] {
set object (i+10) rect front at hammer_x(60*i,-28),hammer_y(60*i,-28) \
size char 3,char 0.8 fc rgb 'white' fs solid 1.0 noborder
set label (i+10) sprintf('%d{\272}',wrapd_label(ra_shift-60*i)) \
offset 0.0,0.1 front center at hammer_x(60*i,-28),hammer_y(60*i,-28) \
font ',10' tc rgb labelcolor
}
do for [i=-1:3] {
set object (i+30) rect front at hammer_x(-150,20*i),hammer_y(-150,20*i) \
size char 3,char 0.8 fc rgb 'white' fs solid 1.0 noborder
set label (i+30) sprintf('%d{\272}',20*i) \
front center at hammer_x(-150,20*i),hammer_y(-150,20*i) \
offset 0.0,0.1 font ',10' tc rgb labelcolor
}

set label 3 '2020-10-05' \
front left at screen 0, screen 1 offset 1,-0.8 tc rgb labelcolor

x(a,b) = hammer_x(wrapd(ra_shift-column(a)),column(b))
y(a,b) = hammer_y(wrapd(ra_shift-column(a)),column(b))
dx(a,b) = x(a+3,b+3)-x(a,b)
dy(a,b) = y(a+3,b+3)-y(a,b)
vx(a,b) = 0.1*dx(a,b)/sqrt(dx(a,b)**2+dy(a,b)**2)
vy(a,b) = 0.1*dy(a,b)/sqrt(dx(a,b)**2+dy(a,b)**2)
plot \
  for [l=-150:150:30] \
    hammer_x(l,180*t-90),hammer_y(l,180*t-90) \
    lw 0.5 dt (4,4) lc rgb 'black' not, \
  for [b=-20:80:20] \
    hammer_x(360*t-180,b),hammer_y(360*t-180,b) \
    lw 0.5 dt (4,4) lc rgb 'black' not, \
  hammer_x(-180,180*t-90),hammer_y(-180,180*t-90) \
  lw 0.5 lc rgb 'black' not, \
  hammer_x(180,180*t-90),hammer_y(180,180*t-90) \
  lw 0.5 lc rgb 'black' not, \
  f u (x(7,8)):(y(7,8)):(vx(7,8)):(vy(7,8)) w vec not lc 1, \
  f u (x(7,8)):($12>0?y(7,8):1/0) w p pt 7 ps 0.3 lc 1 not, \
  f u (x(7,8)):($12>0?1/0:y(7,8)) w p pt 7 ps 0.5 lc rgb 'white' not, \
  f u (x(7,8)):($12>0?1/0:y(7,8)) w p pt 6 ps 0.5 lc 1 not
