#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import numpy as np


def convert_month(elem):
  month = {
    'Jan': '01',  'Feb': '02', 'Mar': '03',  'Apr': '04',
    'May': '05',  'Jun': '06', 'Jul': '07',  'Aug': '08',
    'Sep': '09',  'Oct': '10', 'Nov': '11',  'Dec': '12',
  }
  for k,v in month.items():
    elem = elem.replace(k,v)
  return elem


def parse_datetime(elem):
  items = elem.split()
  return '{}T{}:00'.format(convert_month(items[0]),items[1])


def parse_distance(elem):
  items = elem.split(' | ')
  return list(map(float, items))

if __name__ == '__main__':
  parser = ap()

  parser.add_argument('src')

  args = parser.parse_args()

  albedo = 0.14

  with open(args.src, 'r') as f:
    dummy = f.readline()
    for line in f.readlines():
      items = line.strip().replace('"','').split(',')
      object_name = items[0]
      ca_datetime = parse_datetime(items[1])
      ca_nominal  = parse_distance(items[2])
      ca_minimal  = parse_distance(items[3])
      v_relative  = float(items[4])
      v_infinity  = float(items[5])
      H_magnitude = float(items[6])
      diameter    = 10**(3.1236-0.5*np.log10(albedo)-0.2*H_magnitude)
      object_code = items[8][1:]
      print('{} {} {} {} {} {} {} {} {} {}'.format(
        ca_datetime, H_magnitude, diameter,
        ca_nominal[0], ca_nominal[1],
        ca_minimal[0], ca_minimal[1],
        v_relative, v_infinity, object_code,
      ))
