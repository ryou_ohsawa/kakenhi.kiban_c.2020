#!/usr/bin/env gnuplot
set terminal pdfcairo font 'M+ 1c,12' size 14cm,10cm
set output 'tomoe_neo_detections.pdf'
set enc iso

f = 'tomoe_neo_detections_202010.txt'

set sample 1e3
set xr [0.75:2.95]
set xtics 0,0.5,3 format '%.1f' offset 0,0.3
set mxtics 5
set yr [0:0.65]
set ytics 0,0.1,3 format '%.1f'
set mytics 2
set xlabel '軌道長半径 a (au)' font 'M+ 1c bold,18' offset  0.0,0.4
set ylabel '軌道離心率 e' font 'M+ 1c bold,18' offset -0.7,0.0

plot abs(1.0-1.0/x) dt (16,8) lw 0.5 lc rgb 'black' not, \
     f u 6:8 w p pt 7 ps 0.7 lc 1 not, \
     f u 6:8:(sprintf(" %s&{,}({%.0fm})",strcol(1),column(5))) \
     w labels left offset 0.4,-0.1 rotate by -45 font ',14' not
